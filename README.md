# README #

Home of an abandoned app

### What is this repository for? ###

* A time logging app

### How do I get set up? ###

* Install Python, Django, bootstrap4 and crispy-forms
* Uses Sqlite for the moment, to upgrade to Postgres

### Things to do (if project resumes)

* Postgres db
* Deploy to a proper server e.g. Heroku, PythonAnywhere
* Features to add:  nicer forms, implement statistics (now dummy numbers), friendlier time/date widgets, bug-free data edits, password reset,