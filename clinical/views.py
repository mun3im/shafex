# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Count
from .models import Report, Client, User
from .forms import ReportForm, ClientForm
from django.views.generic import TemplateView, ListView, UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy


@login_required
def report_edit(request, pk):
    report = get_object_or_404(Report, pk=pk)
    if request.method == "POST":
        form = ReportForm(request.POST)
        if form.is_valid():
            report = form.save(commit=False)
            report.author = request.user
            report.save()
            client = Client.objects.filter(pk=pk)
            client.num_cases =  Report.objects.filter(client=pk).count()
            #client.save()
            return redirect('report_detail', pk=report.pk)
    else:
        form = ReportForm(instance=report)
    return render(request, 'clinical/report_edit.html', {'form': form})
#    return render(request, 'clinical/report_list.html', {'reports':reports})

def report_detail(request, pk):
    daysinweek = [ '', 'Isnin', 'Selasa', 'Rabu', 'Khamis', 'Jumaat', 'Sabtu', 'Ahad']
    report = get_object_or_404(Report, pk=pk)
    (tmp,woy,dow) = report.date.isocalendar()
    report.woy = woy
    report.hari = daysinweek[dow]
    num_users = User.objects.count()
#    problem = Problem.objects.all()
    return render(request, 'clinical/report_detail.html', {'report': report})


def stats(request): # system stats
    num_reports = Report.objects.count()
    num_clients = Client.objects.count()
    num_users = User.objects.count()
    stats = {
        'Reports':num_reports,
        'Clients':num_clients,
        'Counselors':num_users,
        }
    return render(request, 'clinical/stats.html', {'stats':stats}, )

@login_required
def home(request):  # personal stats
    reports = Report.objects.filter(author=request.user)
    hits = {}
    num_reports = reports.count()
#    Report.objects.annotate(num_cases=Count(category>1))
    contact_hours = {
        'FIZ':0,'KEW':0,'SOS':0,'PSI':0,'EMO':0,'REM':0,'RTK':0,'MOR':0,'AKA':0,'KER':0,'KUR':0
    }
    return render(request, 'clinical/index.html', {'contact_hours':contact_hours} )

@login_required
def report_delete(request, pk):
    report = get_object_or_404(Report, pk=pk)
    report.delete()
    reports = Report.objects.filter(author=request.user)
    return render(request, 'clinical/report_list.html', {'reports':reports})

@login_required
def report_list(request):
    reports = Report.objects.filter()#[:10]
    num_reports = Report.objects.count()
    return render(request, 'clinical/report_list.html', {'reports':reports, 'num_reports':num_reports})

def client_new(request):
    if request.method == "POST":
        form = ClientForm(request.POST)
        if form.is_valid():
            client = form.save(commit=False)
            client.save()
            return redirect('client_detail', pk=client.pk)
    else:
        form = ClientForm()
    return render(request, 'clinical/client_edit.html', {'form': form})

@login_required
def report_new(request):
    if request.method == "POST":
        form = ReportForm(request.POST)
        if form.is_valid():
            report = form.save(commit=False)
            report.author = request.user
            report.save()
            return redirect('report_detail', pk=report.pk)
    else:
        form = ReportForm()
    return render(request, 'clinical/report_edit.html', {'form': form})

def report_remove(request, pk):
    report = get_object_or_404(report, pk=pk)
    report.delete()
    return redirect('report_list')

def client_detail(request, pk):
    client = get_object_or_404(Client, pk=pk)
#    reports = Report.objects.filter()#[:10]
    client.num_cases =  Report.objects.filter(client=pk).count()
    client.save()
    return render(request, 'clinical/client_detail.html', {'client': client})

def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)

def signin(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('home')
    else:
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request,user) # don't PasswordChangeForm
                    if 'next' in request.GET:
                        return HttpResponseRedirect(request.GET['next'])
                    else:
                        return HttpResponseRedirect('clinical/report_list.html')
                else:
                    messages.add_message(request, messages.ERROR, 'Your account is deactivated.')
                    return render(request, 'registration/login.html')
            else:
                messages.add_message(request, messages.ERROR, 'Username or password invalid.')
                return render(request, 'registration/login.html')
        else:
            return render(request, 'registration/login.html')

def signout(request):
    logout(request)
    return render(request, 'registration/login.html')

def client_list(request):
    clients = Client.objects.filter()
    return render(request, 'clinical/client_list.html', {'clients': clients})

class ClientListView(ListView):
    context_object_name = 'clients'
    template_name = 'clinical/client_list.html'
    model = Client
    num_clients = Client.objects.count()
    queryset = Client.objects.filter()
    num_cases = Report.objects.annotate(num_reports=Count('client'))

class ReportListView(ListView):
    context_object_name = 'reports'
    template_name = 'clinical/report_list.html'
    model = Report
    num_cases = Report.objects.count()

    def get_queryset(self):
#        queryset = super(PostsList, self).get_queryset()
        queryset = Report.objects.filter(author=self.request.user)#[:10]
        return queryset

class ReportUpdate(UpdateView):
    model = Report

class ClientCreate(CreateView):
    model = Client
    fields = '__all__'
    template_name = "clinical/client_create.html"

class ReportCreate(CreateView):
    model = Report
    fields = '__all__'
    template_name = "clinical/report_create.html"

    def get_queryset(self): # tiru yg kat atas
        queryset = Report.objects.filter(author=self.request.user)#[:10]
        return queryset

class  ReportDelete(DeleteView):
    model = Report
    success_url = reverse_lazy('report_list')
