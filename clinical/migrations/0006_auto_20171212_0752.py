# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-12 07:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clinical', '0005_auto_20171210_2305'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='num_cases',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='report',
            name='category',
            field=models.CharField(choices=[('FIZ', 'Masalah Kesihatan dan Pertumbuhan Jasmani'), ('KEW', 'Masalah Kewangan, Keadaan Kehidupan dan Pekerjaan'), ('SOS', 'Masalah Kegiatan Sosial dan Rekreasi'), ('PSI', 'Masalah Perhubungan Psikologi dan Sosial'), ('EMO', 'Masalah Perhubungan Peribadi dan Psikologi'), ('REM', 'Masalah Pergaulan Muda-Mudi, Perkahwinan dan Sekx'), ('RTK', 'Masalah Rumahtangga dan Kekeluargaan'), ('MOR', 'Masalah Moral dan Agama'), ('AKA', 'Masalah Penyesuaian Terhadap Kerja dalam Bilik Kuliah'), ('KER', 'Masalaah Penyesuaian dan Kerjaya Masa Depan'), ('KUR', 'Masalah Kurikulum dan Kaedah Pengajaran')], max_length=3, null=True),
        ),
        migrations.AddField(
            model_name='report',
            name='duration',
            field=models.TimeField(null=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='fakulti',
            field=models.CharField(choices=[('FAB', 'Faculty of Built Environment'), ('FBME', 'Faculty of Biosciences & Medical Engineering'), ('FKA', 'Faculty of Civil Engineering'), ('FKT', 'Faculty of Chemical & Energy Engineering'), ('FKM', 'Faculty of Mechanical Engineering'), ('FGHT', 'Faculty of Geoinformation & Real Estate'), ('FP', 'Faculty of Education'), ('FM', 'Faculty of Management'), ('FS', 'Faculty of Science'), ('FIC', 'Faculty of Islamic Civilization'), ('IBS', 'International Business Schoool'), ('AIS', 'Advanced Informatics School'), ('MJIIT', 'Malaysia-Japan International Institute of Technology'), ('SPACE', 'School of Professional & Continuing Education')], max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='kolej',
            field=models.CharField(choices=[('KTC', 'Kolej Tuanku Canselor'), ('KRP', 'Kolej Rahman Putra'), ('KP', 'Kolej Perdana'), ('KTDI', 'Kolej Tun Dr Ismail'), ('KTF', 'Kolej Tun Fatimah'), ('KTR', 'Kolej Tun Razak'), ('KTHO', 'Kolej Tun Hussein Onn'), ('KDOJ', "Kolej Dato' Onn Jaafar"), ('KDSE', 'Kolej Datin Seri Endon'), ('K9', 'Kolej 9'), ('K10', 'Kolej 10')], max_length=4, null=True),
        ),
        migrations.AlterField(
            model_name='problem',
            name='category',
            field=models.CharField(choices=[('FIZ', 'Masalah Kesihatan dan Pertumbuhan Jasmani'), ('KEW', 'Masalah Kewangan, Keadaan Kehidupan dan Pekerjaan'), ('SOS', 'Masalah Kegiatan Sosial dan Rekreasi'), ('PSI', 'Masalah Perhubungan Psikologi dan Sosial'), ('EMO', 'Masalah Perhubungan Peribadi dan Psikologi'), ('REM', 'Masalah Pergaulan Muda-Mudi, Perkahwinan dan Sekx'), ('RTK', 'Masalah Rumahtangga dan Kekeluargaan'), ('MOR', 'Masalah Moral dan Agama'), ('AKA', 'Masalah Penyesuaian Terhadap Kerja dalam Bilik Kuliah'), ('KER', 'Masalaah Penyesuaian dan Kerjaya Masa Depan'), ('KUR', 'Masalah Kurikulum dan Kaedah Pengajaran')], max_length=3),
        ),
    ]
