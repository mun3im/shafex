# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import timezone
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
import random

KOLEJ_CHOICES = (
    ('KTC',  'Kolej Tuanku Canselor'),
    ('KRP',  'Kolej Rahman Putra'),
    ('KP',   'Kolej Perdana'),
    ('KTDI', 'Kolej Tun Dr Ismail'),
    ('KTF',  'Kolej Tun Fatimah'),
    ('KTR',  'Kolej Tun Razak'),
    ('KTHO', 'Kolej Tun Hussein Onn'),
    ('KDOJ', "Kolej Dato' Onn Jaafar"),
    ('KDSE', 'Kolej Datin Seri Endon'),
    ('K9',   'Kolej 9'),
    ('K10',  'Kolej 10'),
)
FAKULTI_CHOICES = (
    ('FAB',  'Faculty of Built Environment'),
    ('FBME', 'Faculty of Biosciences & Medical Engineering'),
    ('FKA',  'Faculty of Civil Engineering'),
    ('FKT',  'Faculty of Chemical & Energy Engineering'),
    ('FKM',  'Faculty of Mechanical Engineering'),
    ('FGHT', 'Faculty of Geoinformation & Real Estate'),
    ('FP',   'Faculty of Education'),
    ('FM',   'Faculty of Management'),
    ('FS',   'Faculty of Science'),
    ('FIC' , 'Faculty of Islamic Civilization'),
    ('IBS',  'International Business Schoool'),
    ('AIS',  'Advanced Informatics School'),
    ('MJIIT','Malaysia-Japan International Institute of Technology'),
    ('SPACE','School of Professional & Continuing Education')
)

class Client(models.Model):
    name = models.CharField(max_length=100)
    kolej = models.CharField(max_length=4,choices=KOLEJ_CHOICES,null=True)
    fakulti = models.CharField(max_length=5, choices=FAKULTI_CHOICES, null=True)
    nric = models.IntegerField(null=True)
    email = models.EmailField(null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

CATEGORY_CHOICES = (
    ('FIZ', 'Masalah Kesihatan dan Pertumbuhan Jasmani'),
    ('KEW', 'Masalah Kewangan, Keadaan Kehidupan dan Pekerjaan'),
    ('SOS', 'Masalah Kegiatan Sosial dan Rekreasi'),
    ('PSI', 'Masalah Perhubungan Psikologi dan Sosial'),
    ('EMO', 'Masalah Perhubungan Peribadi dan Psikologi'),
    ('REM', 'Masalah Pergaulan Muda-Mudi, Perkahwinan dan Sekx'),
    ('RTK', 'Masalah Rumahtangga dan Kekeluargaan'),
    ('MOR', 'Masalah Moral dan Agama'),
    ('AKA', 'Masalah Penyesuaian Terhadap Kerja dalam Bilik Kuliah'),
    ('KER', 'Masalaah Penyesuaian dan Kerjaya Masa Depan'),
    ('KUR', 'Masalah Kurikulum dan Kaedah Pengajaran'),
)

class Problem(models.Model):
    code = models.IntegerField(primary_key=True,default=1)
    category = models.CharField(choices=CATEGORY_CHOICES,max_length=3)
    description = models.CharField(max_length=100)
#    major = models.IntegerField(unique=True,default=1)

    def __str__(self):
        return str(self.code)

class Report(models.Model):
    author = models.ForeignKey(User)
    client = models.ForeignKey(Client,null=True)
    date = models.DateField(default=timezone.now)
    time_in = models.TimeField(default=timezone.now)
    time_out = models.TimeField(default=timezone.now)
    duration = models.TimeField(null=True)
    category = models.CharField(choices=CATEGORY_CHOICES,max_length=3,null=True)
    problem_code = models.ForeignKey(Problem, null=True)
#    problem_code = models.IntegerField(null=True)
    summary = models.CharField(max_length=500, null=True)
    goal_a = models.CharField(max_length=50, blank=True, null=True)
    goal_b = models.CharField(max_length=50, blank=True,  null=True)
    goal_c = models.CharField(max_length=50, blank=True, null=True)
    diagnosis = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.summary

    def get_absolute_url(self):
        return reverse('report_detail', kwargs={'pk':self.pk})

    class Meta:
        ordering = ['-date','-time_in']
