# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from clinical import models

admin.site.register(models.Report)
admin.site.register(models.Client)
admin.site.register(models.Problem)
