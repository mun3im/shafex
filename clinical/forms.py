from django import forms
from django.core.exceptions import ValidationError
from django.forms.extras.widgets import  SelectDateWidget
from django.forms.widgets import DateInput, TimeInput
from django.forms.formsets import BaseFormSet, formset_factory
from django.contrib.admin.widgets import AdminTimeWidget, AdminDateWidget
from django.core.urlresolvers import reverse
from . import models
#from suit.widgets import AutosizedTextarea, SuitDateWidget, SuitTimeWidget
from bootstrap3.tests import TestForm
import random
from datetime import datetime, timedelta
from django.utils import timezone
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, Div, Fieldset
from crispy_forms.bootstrap import Field, InlineRadios, TabHolder, Tab, FormActions





class ReportForm(forms.ModelForm):
    date = forms.DateField(widget=SelectDateWidget, initial=timezone.now())
    summary = forms.CharField(widget=forms.Textarea)
    diagnosis = forms.CharField(widget=forms.Textarea)
    time_in = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))
    time_out = forms.TimeField(widget=forms.TimeInput(format='%H:%M'))
#    timein = date.replace(hour=time_in.hour,minute=time_in.minute)
    FMT = '%H:%M'
#    masuk = time_in.time.hour*60 + time_in.time.minute
#    masuk = time_in.time()
#    duration = time_out.time() - time_in.time()
#    duration = datetime.strptime(time_out, FMT).time() - datetime.strptime(time_in, FMT).time()

    def __init__(self, *args, **kwargs):
        super(ReportForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id_report_form'
        self.helper.form_method = 'post'
        self.helper.form_action = ('report_new')
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-primary'))
        self.helper.form_class = 'form-horizontal'
#        self.helper.layout = Layout(
#           Fieldset('Administriva', Field('author', 'client', 'date', 'time_in', 'time_out')),
#           Fieldset('Session', Field('summary', 'goal_a', 'goal_b', 'goal_c', 'diagnosis')))

    class Meta:
        model = models.Report
        exclude = {'author','duration','category','problem'}


class ClientForm(forms.ModelForm):
    fakulti = forms.ChoiceField(required=False, widget=forms.Select)
    kolej = forms.ChoiceField(required=False, widget=forms.Select)

    def __init__(self, *args, **kwargs):
        super(ClientForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id_client_form'
        self.helper.form_method = 'post'
        self.helper.form_action = ('client_new')
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-primary'))
        self.helper.form_class = 'form-horizontal'

    class Meta:
        model = models.Client
        exclude = {'nric'}
