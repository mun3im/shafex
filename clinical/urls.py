from django.conf.urls import url, include
from . import views
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^about/$', TemplateView.as_view(template_name="etc/about.html")),
    url(r'^contact/$', TemplateView.as_view(template_name="etc/contact.html")),
    url(r'^help/$', TemplateView.as_view(template_name="etc/help.html")),
    url(r'^settings/$', TemplateView.as_view(template_name="etc/settings.html")),
    url(r'^profile/$', TemplateView.as_view(template_name="etc/profile.html")),
    url(r'^stats/$', views.stats, name='stats'),
    url(r'^reports/$', views.ReportListView.as_view(), name='report_list'),
    url(r'^clients/$', views.ClientListView.as_view()),
    url(r'^report/(?P<pk>\d+)/$', views.report_detail, name='report_detail'),
    url(r'^report/new/$', views.report_new, name='report_new'),
    url(r'^report/(?P<pk>\d+)/edit/$', views.report_edit, name='report_edit'),
#    url(r'^report/(?P<pk>\d+)/edit/$', views.ReportUpdate.as_view(), name='ReportUpdate'),
    url(r'^client/new/$', views.client_new, name='client_new'),
    url(r'^client/(?P<pk>\d+)/$', views.client_detail, name='client_detail'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^client/create/$', views.ClientCreate.as_view(), name='client_create'),
    url(r'^report/create/$', views.ReportCreate.as_view(), name='report_create'),
    url(r'^report/(?P<pk>\d+)/delete/$', views.ReportDelete.as_view(), name='report_delete'),
]
